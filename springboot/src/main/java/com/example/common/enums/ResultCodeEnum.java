package com.example.common.enums;

public enum ResultCodeEnum {
    SUCCESS("200", "成功"),

    PARAM_ERROR("400", "参数异常"),
    TOKEN_INVALID_ERROR("401", "无效的token"),
    TOKEN_CHECK_ERROR("401", "token验证失败，请重新登录"),
    PARAM_LOST_ERROR("4001", "参数缺失"),

    SYSTEM_ERROR("500", "系统异常"),
    USER_EXIST_ERROR("5001", "用户名已存在"),
    USER_NOT_LOGIN("5002", "用户未登录"),
    USER_ACCOUNT_ERROR("5003", "账号或密码错误"),
    USER_NOT_EXIST_ERROR("5004", "用户不存在"),
    PARAM_PASSWORD_ERROR("5005", "原密码输入错误"),
    AUTH_ERROR("5006", "审核通过无法更改"),
    ORDERS_STATUS_ERROR("5007", "订单无法接单"),
    ORDERS_AUTH_ERROR("5007", "非骑手无权限接单"),
    ACCOUNT_LIMIT("5008", "余额不足"),
    ACCEPT_ERROR("5008", "骑手无法接自己下的订单"),
    ROLE_ERROR("5009","角色错误"),
    CERTIFICATION_ERROR("5010","您已经认证过骑手")
    ;

    public String code;
    public String msg;

    ResultCodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
