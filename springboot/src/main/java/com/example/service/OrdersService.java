package com.example.service;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import com.example.common.enums.OrderStatus;
import com.example.common.enums.RecordsTypeEnum;
import com.example.common.enums.ResultCodeEnum;
import com.example.common.enums.RoleEnum;
import com.example.entity.*;
import com.example.exception.CustomException;
import com.example.mapper.OrdersMapper;
import com.example.utils.TokenUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 订单信息业务处理
 **/
@Service
public class OrdersService {

    @Resource
    private OrdersMapper ordersMapper;

    @Resource
    private CertificationService certificationService;

    @Resource
    private AddressService addressService;

    @Resource
    private UserService userService;

    @Resource
    private RecordsService recordsService;

    /*** 用户下单，新增到数据库*/
    public void addOrder(Orders orders) {
//        TokenUtils该工具类用于生成token和解析token对象
        Account currentUser = TokenUtils.getCurrentUser();//当前登录的用户信息
        User user = userService.selectById(currentUser.getId());
        BigDecimal account = user.getAccount();//获取账户余额
        if (orders.getPrice() > account.doubleValue()) {//orders.getPrice()订单金额，account.doubleValue()转换成double类型的值
            throw new CustomException(ResultCodeEnum.ACCOUNT_LIMIT);//账户余额不足
        }
        //更新账户余额（扣钱）
        user.setAccount(user.getAccount().subtract(BigDecimal.valueOf(orders.getPrice())));//账户余额减去订单金额
        userService.updateById(user);

        orders.setUserId(currentUser.getId());//设置用户的id
        orders.setOrderNo(IdUtil.getSnowflakeNextIdStr());//设置唯一的订单编号
        orders.setTime(DateUtil.now());//设置当前的下单时间
        orders.setStatus(OrderStatus.NO_ACCEPT.getValue());//设置订单状态为‘待接单’
        ordersMapper.insert(orders);//新增到数据库
        //记录收支明细
        RecordsService.record("下单--" + orders.getName(), BigDecimal.valueOf(orders.getPrice()), RecordsTypeEnum.OUT.getValue());
     }

//新增
    public void add(Orders orders) {
        ordersMapper.insert(orders);
    }

    /*** 删除*/
    public void deleteById(Integer id) {
        ordersMapper.deleteById(id);
    }

    /*** 批量删除*/
    public void deleteBatch(List<Integer> ids) {
        for (Integer id : ids) {
            ordersMapper.deleteById(id);
        }
    }

    /*** 修改*/
    @Transactional
    public void updateById(Orders orders) {
        if (OrderStatus.NO_RECEIVE.getValue().equals(orders.getStatus())) {
        //  骑手确认送达订单后，给骑手打钱
            Integer acceptId = orders.getAcceptId();
            User user = userService.selectById(acceptId);
            user.setAccount(user.getAccount().add(BigDecimal.valueOf(orders.getPrice())));
            //记录收支明细
            RecordsService.record("接单--" + orders.getName(), BigDecimal.valueOf(orders.getPrice()), RecordsTypeEnum.INCOME.getValue());
        } else if (OrderStatus.CANCEL.getValue().equals(orders.getStatus())) {
            //记录收支明细
            RecordsService.record("取消订单--" + orders.getName(), BigDecimal.valueOf(orders.getPrice()), RecordsTypeEnum.CANCEL.getValue());

        }
        ordersMapper.updateById(orders);
    }



    /*** 根据ID查询*/
    public Orders selectById(Integer id) {
        Orders orders = ordersMapper.selectById(id);
        //订单查询关联地址查询

        //取货地址
        Address address = addressService.selectById(orders.getAddressId());
        orders.setAddress(address);
        //收货地址
        Address targetAddress = addressService.selectById(orders.getTargetId());
        orders.setTargetAddress(targetAddress);

        if (orders.getAcceptId() != null) {
            Certification certification = certificationService.selectByUserId(orders.getAcceptId());
            orders.setAcceptUser(certification);//为订单中关联认证骑手的信息
        }
        return orders;
    }

    /*** 查询所有*/
    public List<Orders> selectAll(Orders orders) {
        List<Orders> ordersList = ordersMapper.selectAll(orders);
        for (Orders o : ordersList) {//for循环所有的订单
            String time = o.getTime();//获取到下单时间
            long between = DateUtil.between(DateUtil.parseDateTime(time), new Date(), DateUnit.MINUTE);//求两个参数的时间差，当前时间减去下单时间
            o.setMinutes(between);
        }
        return ordersList;
    }

    /**
     * 分页查询
     */
    public PageInfo<Orders> selectPage(Orders orders, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Orders> list = ordersMapper.selectAll(orders);
        return PageInfo.of(list);
    }

    public void accept(Orders orders) {
        Integer ordersId = orders.getId();
        orders = selectById(ordersId);
        if (!OrderStatus.NO_ACCEPT.getValue().equals(orders.getStatus())) {
            throw new CustomException(ResultCodeEnum.ORDERS_STATUS_ERROR);
        }
        Account currentUser = TokenUtils.getCurrentUser();
        if (currentUser.getId().equals(orders.getUserId())) {  // 自己没法接单
            throw new CustomException(ResultCodeEnum.ACCEPT_ERROR);
        }
        Certification certification = certificationService.selectByUserId(currentUser.getId());
        if (RoleEnum.USER.name().equals(currentUser.getRole()) && (certification == null || !"通过".equals(certification.getStatus()))) {
            throw new CustomException(ResultCodeEnum.ORDERS_AUTH_ERROR);
        }
        orders.setAcceptId(currentUser.getId());
        orders.setAcceptTime(DateUtil.now());
        orders.setStatus(OrderStatus.NO_ARRIVE.getValue());
        this.updateById(orders);
    }


}