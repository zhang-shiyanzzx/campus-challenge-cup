package com.example.service;

import cn.hutool.core.date.DateUtil;
import com.example.entity.Account;
import com.example.entity.Records;
import com.example.mapper.RecordsMapper;
import com.example.utils.TokenUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import javax.annotation.Resource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * 收支明细业务处理
 **/
@Service
public class RecordsService implements InitializingBean {

    @Resource
    private RecordsMapper recordsMapper;
    private static RecordsMapper staticRecordsMapper;

   //全局静态方法-----收支明细存储
    public static void record(String content, BigDecimal money, String type) {
        Records records = new Records();
        Account currentUser = TokenUtils.getCurrentUser();//根据tokenutils解析，获取当前用户的信息
        records.setUserId(currentUser.getId());
        records.setTime(DateUtil.now());
        records.setMoney(money);
        records.setType(type);
        records.setContent(content);
        staticRecordsMapper.insert(records);//插入到record表中
    }

    /**
     * 新增
     */
    public void add(Records records) {
        recordsMapper.insert(records);
    }

    /**
     * 删除
     */
    public void deleteById(Integer id) {
        recordsMapper.deleteById(id);
    }

    /**
     * 批量删除
     */
    public void deleteBatch(List<Integer> ids) {
        for (Integer id : ids) {
            recordsMapper.deleteById(id);
        }
    }

    /**
     * 修改
     */
    public void updateById(Records records) {
        recordsMapper.updateById(records);
    }

    /**
     * 根据ID查询
     */
    public Records selectById(Integer id) {
        return recordsMapper.selectById(id);
    }

    /**
     * 查询所有
     */
    public List<Records> selectAll(Records records) {
        return recordsMapper.selectAll(records);
    }

    /**
     * 分页查询
     */
    public PageInfo<Records> selectPage(Records records, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Records> list = recordsMapper.selectAll(records);
        return PageInfo.of(list);
    }

    @Override
    //赋值静态变量
    public void afterPropertiesSet() throws Exception {
        staticRecordsMapper = recordsMapper;
    }
}