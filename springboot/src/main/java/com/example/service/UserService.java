package com.example.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.example.common.Constants;
import com.example.common.enums.RecordsTypeEnum;
import com.example.common.enums.ResultCodeEnum;
import com.example.common.enums.RoleEnum;
import com.example.entity.Account;
import com.example.entity.Certification;
import com.example.entity.User;
import com.example.exception.CustomException;
import com.example.mapper.UserMapper;
import com.example.utils.TokenUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/*** 用户表业务处理**/
@Service
public class UserService {
    @Resource
    private UserMapper userMapper;
    @Resource
    private CertificationService certificationService;
    @Resource
    private RecordsService recordsService;
    /*** 新增（注册新增到user表中）*/
    public void add(User user) {
        User dbUser = this.selectByUsername(user.getUsername());//新增之前先根据用户传递过来的用户名进行查询
        if (ObjectUtil.isNotNull(dbUser)){//判断用户名是否重复
            throw new CustomException(ResultCodeEnum.USER_EXIST_ERROR);//用户名已存在
        }
        if (ObjectUtil.isEmpty(user.getPassword())){
            user.setPassword(Constants.USER_DEFAULT_PASSWORD);//设置默认密码为123
        }  if (ObjectUtil.isEmpty(user.getName())){
            user.setName(user.getUsername());//将用户号设置为用户名称
        }
        user.setRole(RoleEnum.USER.name());//设置角色为用户
        userMapper.insert(user);//插入到数据库
    }
    //根据用户名查询用户
    public User selectByUsername(String username) {
        User user = new User();//新建一个user对象
        user.setUsername(username);//为user对象设置用户名
        List<User> userList = this.selectAll(user);//调用selectAll查询所有方法（只传入用户名），返回的userList只有一个元素
        return CollUtil.isEmpty(userList) ? null : userList.get(0);//userList.get(0) 因为username是唯一的
    }

    /*** 删除*/
    public void deleteById(Integer id) {
        userMapper.deleteById(id);
    }

    /*** 批量删除*/
    public void deleteBatch(List<Integer> ids) {
        for (Integer id : ids) { //for循环遍历
            userMapper.deleteById(id);
        }
    }
    /*** 修改*/
    public void updateById(User user) {
        userMapper.updateById(user);
    }
    /*** 根据ID查询*/
    public User selectById(Integer id) {
        return userMapper.selectById(id);
    }
    /*** 查询所有*/
    public List<User> selectAll(User user) {
        return userMapper.selectAll(user);
    }
    /*** 分页查询*/
    public PageInfo<User> selectPage(User user, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<User> list = userMapper.selectAll(user);
        return PageInfo.of(list);
    }


    /*** 用户登录*/
    public Account login(Account account) {
        Account dbUser = this.selectByUsername(account.getUsername());//根据用户名进行查询
        if (ObjectUtil.isNull(dbUser)) {//判断用户名是否为空
            throw new CustomException(ResultCodeEnum.USER_NOT_EXIST_ERROR);
        }
        if (!account.getPassword().equals(dbUser.getPassword())) {   // 比较用户输入密码和数据库密码是否一致
            throw new CustomException(ResultCodeEnum.USER_ACCOUNT_ERROR);
        }
        // 生成token
        String tokenData = dbUser.getId() + "-" + RoleEnum.USER.name();//拼接
        String token = TokenUtils.createToken(tokenData, dbUser.getPassword());//生成token
        dbUser.setToken(token);//为dbUser设置Token字段，并设置值为token
        // 查询关联的认证信息
        Certification certification = certificationService.selectByUserId(dbUser.getId());//查询当前登陆用户的骑手认证信息
        dbUser.setIsRider(ObjectUtil.isNotNull(certification) && "通过".equals(certification.getStatus()));
        return dbUser;//返回给前端数据库中的用户名和密码 以及 token
    }

    public void charge(BigDecimal money) {
        Account currentUser = TokenUtils.getCurrentUser();//获取当前用户信息
        currentUser.setAccount(currentUser.getAccount().add(money));
        this.updateById((User) currentUser);
       //记录收支明细
        RecordsService.record("充值",money, RecordsTypeEnum.CHARGE.getValue());
    }
//用户注册
    public void register(Account account) {
        User user = new User();
        BeanUtils.copyProperties(account, user);//把传递过来的account复制到user对象中
        this.add(user);
    }

    public void updatePassword(Account account) {
        User dbUser = userMapper.selectByUsername(account.getUsername());
        if (ObjectUtil.isNull(dbUser)) {
            throw new CustomException(ResultCodeEnum.USER_NOT_EXIST_ERROR);
        }
        if (!account.getPassword().equals(dbUser.getPassword())) {
            throw new CustomException(ResultCodeEnum.PARAM_PASSWORD_ERROR);
        }
        dbUser.setPassword(account.getNewPassword());
        userMapper.updateById(dbUser);
    }
}