package com.example.service;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.example.common.enums.OrderStatus;
import com.example.common.enums.RecordsTypeEnum;
import com.example.entity.Orders;
import com.example.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
//定时任务
@Component//表示是一个Spring的组件
public class OrderSchedule {

    private static final Logger log = LoggerFactory.getLogger(OrderSchedule.class);

    @Resource
    private OrdersService ordersService;
    @Resource
    private UserService userService;


    @Scheduled(fixedRate = 10 * 60 * 1000)//定时任务注解，每个这么长时间执行一次
    public void task() {
        log.info("======================订单扫描定时任务开始执行======================");
        Orders params = new Orders();
        params.setStatus(OrderStatus.NO_ACCEPT.getValue());//设置状态为待接单
        // 查询出待接单的列表
        List<Orders> ordersList = ordersService.selectAll(params);
        //进行遍历
        ordersList.forEach(orders -> {
            // 秒
            long between = DateUtil.between(DateUtil.parseDateTime(orders.getTime()), new Date(), DateUnit.SECOND);//计算下单时间和当前时间的间隔
//            DateUtil.parseDateTime(orders.getTime())订单下单时间转换为时间类型，并和当前时间做差， DateUnit.SECOND时间类型为秒
            if (between > 600) {   // 超过5分钟未接单  自动取消
                orders.setStatus(OrderStatus.CANCEL.getValue());//设置状态为取消订单
                ordersService.updateById(orders);

                //订单取消后需要归还用户金额
                User user = userService.selectById(orders.getUserId());//根据orders的userId字段查询出用户的信息
                user.setAccount(user.getAccount().add(BigDecimal.valueOf(orders.getPrice())));//归还给用户金额
                userService.updateById(user);

//记录收支明细
                RecordsService.record("取消订单--" + orders.getName(), BigDecimal.valueOf(orders.getPrice()), RecordsTypeEnum.CANCEL.getValue());

                log.info("======================订单【" + orders.getOrderNo() + "】已取消======================");
            }
        });
        log.info("======================订单扫描定时任务执行结束======================");
    }

}
