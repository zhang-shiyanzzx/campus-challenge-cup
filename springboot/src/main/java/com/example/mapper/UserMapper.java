package com.example.mapper;

import com.example.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;
import java.util.List;

/*** 操作user相关数据接口*/
public interface UserMapper {

    /*** 新增*/
    int insert(User user);
    /*** 删除*/
    int deleteById(Integer id);
    /*** 修改*/
    int updateById(User user);
    /*** 根据ID查询*/
    User selectById(Integer id);
    /*** 查询所有*/
    List<User> selectAll(User user);

    @Update("update user set account = account + #{money} where id = #{id}")
    void charge(@Param("id") Integer id, @Param("money") BigDecimal money);
  //登录时根据用户名进行查询
    @Select("select * from user where username = #{username}")
    User selectByUsername(String username);
}