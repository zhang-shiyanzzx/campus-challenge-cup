package com.example.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.example.common.Result;
import com.example.common.enums.ResultCodeEnum;
import com.example.common.enums.RoleEnum;
import com.example.entity.Account;
import com.example.service.AdminService;
import com.example.service.CertificationService;
import com.example.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 基础前端接口
 */
@RestController
public class WebController {

    @Resource
    private AdminService adminService;
    @Resource
    private UserService userService;

    @GetMapping("/")
    public Result hello() {
        return Result.success("访问成功");
    }

    /*** 登录*/
    @PostMapping("/login")
    public Result login(@RequestBody Account account) {
        if (ObjectUtil.isEmpty(account.getUsername()) || ObjectUtil.isEmpty(account.getPassword())
                || ObjectUtil.isEmpty(account.getRole())) {
            return Result.error(ResultCodeEnum.PARAM_LOST_ERROR);//缺少参数
        }
        if (RoleEnum.ADMIN.name().equals(account.getRole())) {//管理员登录
            account = adminService.login(account);
        } else if (RoleEnum.USER.name().equals(account.getRole())) {//用户登录
            account = userService.login(account);
        }else {
            return  Result.error(ResultCodeEnum.USER_NOT_EXIST_ERROR);//用户不存在
        }

        return Result.success(account);//Result.success(account)是静态方法，静态方法可以通过类名直接调用，无需创建类的实例。
    }

    /*** 注册*/
    @PostMapping("/register")
    public Result register(@RequestBody Account account) {
        if (StrUtil.isBlank(account.getUsername()) || StrUtil.isBlank(account.getPassword())
                || ObjectUtil.isEmpty(account.getRole())) {
            return Result.error(ResultCodeEnum.PARAM_LOST_ERROR);
        }
        if (RoleEnum.ADMIN.name().equals(account.getRole())) {//管理员注册
            adminService.register(account);
        } else if (RoleEnum.USER.name().equals(account.getRole())) {//用户注册
            userService.register(account);
        }else {
            return Result.error(ResultCodeEnum.ROLE_ERROR);//角色错误
        }
        return Result.success();
    }

    /*** 修改密码*/
    @PutMapping("/updatePassword")
    public Result updatePassword(@RequestBody Account account) {
        if (StrUtil.isBlank(account.getUsername()) || StrUtil.isBlank(account.getPassword())
                || ObjectUtil.isEmpty(account.getNewPassword())) {
            return Result.error(ResultCodeEnum.PARAM_LOST_ERROR);
        }
        if (RoleEnum.ADMIN.name().equals(account.getRole())) {//管理员修改密码
            adminService.updatePassword(account);
        } else if (RoleEnum.USER.name().equals(account.getRole())) {//用户修改密码
            userService.updatePassword(account);
        }
        return Result.success();
    }

}
