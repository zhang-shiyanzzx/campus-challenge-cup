package com.example.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/*** 订单信息*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Orders implements Serializable {
    private static final long serialVersionUID = 1L;

    /** ID */
    private Integer id;
    /** 订单编号 */
    private String orderNo;
    /** 物品名称 */
    private String name;
    private String descr;
    /** 物品图片 */
    private String img;
    /** 物品类型 */
    private String type;
    /** 物品重量 */
    private Double weight;
    /** 小费 */
    private Double price;
    /** 发起人ID */
    private Integer userId;
    /** 接单人ID */
    private Integer acceptId;
    /** 创建时间 */
    private String time;
    /** 接单时间 */
    private String acceptTime;
    /** 订单状态 */
    private String status;
    /** 发起人联系ID */
    private Integer addressId;
    /** 到达处联系ID */
    private Integer targetId;
    /** 订单备注 */
    private String comment;
    //下单人名称
    private String userName;
    //接单人名称
    private String acceptName;

    // 取货地址
    private Address address;
    // 收货地址
    private Address targetAddress;

    // 接单人-骑手信息
    private Certification acceptUser;
    // 下单时间
    private long minutes;

}