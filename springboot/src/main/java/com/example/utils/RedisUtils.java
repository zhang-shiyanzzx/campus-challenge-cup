package com.example.utils;

import org.springframework.data.redis.connection.RedisConnectionCommands;
import org.springframework.data.redis.connection.RedisServerCommands;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;
@SuppressWarnings(value = {"unchecked"})
@Component
public class RedisUtils {
    private  static RedisTemplate<String,Object> staticRedisTemplate;
    private  final  RedisTemplate<String,Object> redisTemplate;
    public RedisUtils(RedisTemplate<String,Object> redisTemplate){
        this.redisTemplate = redisTemplate;
    }
    //Springboot启动成功之后会调用这个方法
    @PostConstruct
    public void initRedis(){
        //初始化配置 静态staticRedisTemplate对象，方便后续操作数据
        staticRedisTemplate = redisTemplate;
    }
    public static <T> void setCacheObject(final String key,final  T value){
        staticRedisTemplate.opsForValue().set(key,value);
    }
    public static <T> void  setCacheObject(final String key,final  T value,final long timeout,final TimeUnit timeUnit){
     staticRedisTemplate.opsForValue().set(key,value,timeout,timeUnit);
    }

public static <T>  T getCacheObject(final String key){
        return (T) staticRedisTemplate.opsForValue().get(key);
}
public  static boolean deleteObject(final String key){
        return Boolean.TRUE.equals(staticRedisTemplate.delete(key));
}
public  static Long getExpireTime(final String key){
        return staticRedisTemplate.getExpire(key);
}
public  static void ping(){
        String res = staticRedisTemplate.execute(RedisConnectionCommands::ping);
}

}
